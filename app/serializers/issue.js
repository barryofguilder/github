import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  primaryKey: 'number',

  normalize(modelClass, responseHash, prop) {
    let commentsCount = responseHash.comments;
    delete responseHash.comments;

    responseHash.commentsCount = commentsCount;

    responseHash.links = {
      comments: responseHash.comments_url
    };
    
    return this._super(modelClass, responseHash, prop);
  }
});
