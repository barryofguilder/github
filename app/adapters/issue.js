import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  buildURL(modelName, id, snapshot/*, requestType, query*/) {
    let repo = snapshot.belongsTo('repo');
    return `${this.urlPrefix()}/repos/${repo.id}/issues/${id}`;
  }
});
