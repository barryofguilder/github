import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';

export default Model.extend({
  issue: belongsTo('issue'),

  url: attr(),
  htmlUrl: attr(),
  issueUrl: attr(),
  user: attr(),
  createdAt: attr(),
  updatedAt: attr(),
  body: attr()
});
