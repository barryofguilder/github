import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'img',
  attributeBindings: ['imgSource:src'],

  avatarUrl: null,
  imgSource: Ember.computed('avatarUrl', function() {
    let avatarUrl = this.get('avatarUrl');

    if (Ember.isEmpty(avatarUrl)) {
      avatarUrl = 'http://www.gravatar.com/avatar/94d093eda664addd6e450d7e9881bcad?s=64&d=identicon&r=PG';
    }

    return avatarUrl;
  })
});
